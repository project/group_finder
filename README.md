CONTENTS OF THIS FILE
---------------------

* Introduction
* Predefined finders
* Features

INTRODUCTION
------------

This module allows you to find [group](https://www.drupal.org/project/group) in different scenarios.


PREDEFINED FINDERS
------------

 - Create new group
 - Group by content
 - Group route
 - Group URL query


FEATURES
------------

 - Find group in different contexts other than the group route.
 - Alter/Override predefined finders according to your needs.
 - Create your own finders.
